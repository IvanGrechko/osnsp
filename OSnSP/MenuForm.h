#pragma once

#include <Windows.h>

struct MenuItem
{
	int ID;
	LPCSTR Text;
};

void AddMenuItemWithSubMenu(HMENU hMenu, HMENU subMenu, MenuItem item)
{
	AppendMenu(hMenu, MF_STRING | MF_POPUP, (UINT_PTR)subMenu, item.Text);
}

void AddMenuItem(HMENU hMenu, MenuItem item)
{
	AppendMenu(hMenu, MF_STRING, item.ID, item.Text);
}
