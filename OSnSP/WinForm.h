#pragma once
#include <Windows.h>
#include <strsafe.h>
#include <stdio.h>

LPCSTR className = "asdsad";

ATOM RegisterClass(HINSTANCE hInstance, WNDPROC winProc)
{
	WNDCLASSEX wec;
	wec.cbSize = sizeof(WNDCLASSEX);
	wec.style = 0;
	wec.cbClsExtra = 0;
	wec.cbWndExtra = 0;
	wec.hInstance = hInstance;
	wec.hIcon = nullptr;
	wec.hCursor = nullptr;
	wec.hbrBackground = 0;
	wec.lpszMenuName = NULL;
	wec.lpszClassName = className;
	wec.hIconSm = NULL;
	wec.lpfnWndProc = winProc;

	return RegisterClassEx(&wec);
}

HWND CreateCustomWindow(LPCTSTR lpWindowName, HINSTANCE hInstance, HWND parent)
{
	return CreateWindow(
		className,
		lpWindowName,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		NULL,
		CW_USEDEFAULT,
		NULL,
		parent,
		NULL,
		hInstance,
		NULL);
}

void ShowUpdateWindows(HWND hMainWnd, int nCmdShow)
{
	ShowWindow(hMainWnd, nCmdShow);
	UpdateWindow(hMainWnd);
}

int InvokeMessageLoop()
{
	MSG msg;
	while (GetMessage(&msg, NULL, NULL, NULL)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

int ShowError(HWND hWnd, LPCSTR text)
{
	DWORD error = GetLastError();
	char buffer[50];
	sprintf_s(buffer, "%s.\n\r Error Code: %d", text, error);
	return MessageBox(hWnd, buffer, "Error", MB_OK);
}
