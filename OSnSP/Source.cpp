#include <Windows.h>
#include "WinForm.h"
#include "MenuForm.h"
#include "SourceItems.h"

LRESULT CALLBACK WndProc(_In_ HWND   hwnd, _In_ UINT   uMsg, _In_ WPARAM wParam, _In_ LPARAM lParam);
void CreateMainMenu(HWND hWnd);
void ProcMenu(HWND hWnd, WORD wParam);
void ShowProcessInfo(ProcessInfo* info);
HINSTANCE Instance;

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPreviousInst, LPSTR lpCommandLine, int nCommandShow)
{
	Instance = hInst;
	if (!RegisterClass(hInst, WndProc))
	{
		ShowError(NULL, "Cant register class");
		return GetLastError();
	}

	HWND mainWindow = CreateCustomWindow(("Lab1"), hInst, NULL);
	if (!mainWindow)
	{
		ShowError(NULL, "Cant create window");
		return GetLastError();
	}
	CreateMainMenu(mainWindow);

	ShowUpdateWindows(mainWindow, nCommandShow);
	return InvokeMessageLoop();
}

LRESULT CALLBACK WndProc(_In_ HWND   hWnd, _In_ UINT   uMsg, _In_ WPARAM wParam, _In_ LPARAM lParam)
{
	PAINTSTRUCT ps;
	switch (uMsg) {
	case WM_PAINT:
		BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		break;
	case WM_COMMAND:
		ProcMenu(hWnd, LOWORD(wParam));
		break;
	case WM_DESTROY:
		PostQuitMessage(NULL);
		break;
	default:
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}
	return NULL;
}

void CreateMainMenu(HWND hWnd)
{
	HMENU menu = CreateMenu();
	HMENU processMenu = CreateMenu();
	HMENU infoMenu = CreateMenu();
	HMENU nProcessMenu = CreateMenu();
	HMENU nInfoMenu = CreateMenu();

	AddMenuItemWithSubMenu(menu, processMenu, MainMenu.Processes);
	AddMenuItemWithSubMenu(menu, infoMenu, MainMenu.Info);

	AddMenuItemWithSubMenu(processMenu, nProcessMenu, ProcessMenu.Notepad);
	AddMenuItem(nProcessMenu, ProcessNotepad.Notepad);
	AddMenuItem(nProcessMenu, ProcessNotepad.NotepadWithText);
	AddMenuItem(nProcessMenu, ProcessNotepad.NotepadWithCustomText);

	AddMenuItem(processMenu, ProcessMenu.Calculator);
	AddMenuItem(processMenu, ProcessMenu.Close);

	AddMenuItem(infoMenu, InfoMenu.CurrentProcess);
	AddMenuItemWithSubMenu(infoMenu, nInfoMenu, InfoMenu.Notepad);
	AddMenuItem(nInfoMenu, InfoNotepad.Notepad);
	AddMenuItem(nInfoMenu, InfoNotepad.NotepadWithText);
	AddMenuItem(nInfoMenu, InfoNotepad.NotepadWithCustomText);

	AddMenuItem(infoMenu, InfoMenu.Calculator);
	
	SetMenu(hWnd, menu);
}

void ProcMenu(HWND hWnd, WORD wParam)
{
	if (wParam >= 10 && wParam <= 19)
	{
		int index = wParam % 10;
		ProcessInfo* info = ProcessInfos[index];

		PROCESS_INFORMATION processInformation;
		STARTUPINFO sInfo;
		sInfo.cb = sizeof(STARTUPINFO);
		sInfo.lpReserved = NULL;
		sInfo.lpTitle = NULL;
		sInfo.dwX = CW_USEDEFAULT;
		sInfo.dwY = CW_USEDEFAULT;
		sInfo.dwXSize = CW_USEDEFAULT;
		sInfo.dwYSize = CW_USEDEFAULT;
		sInfo.dwFlags = SW_SHOWDEFAULT;
		sInfo.cbReserved2 = 0;
		sInfo.lpReserved2 = 0;
		sInfo.lpDesktop = nullptr;
		sInfo.hStdInput = nullptr;
		sInfo.hStdOutput = nullptr;
		sInfo.hStdError = nullptr;

		if (wParam == ProcessMenu.Close.ID)
		{
			ProcessInfo* calcInfo = ProcessInfos[1];
			if (calcInfo->ProcessHandle != nullptr)
			{
				if (TerminateProcess(calcInfo->ProcessHandle, 0))
				{
					calcInfo->ProcessHandle = nullptr;
					calcInfo->ThreadHandle = nullptr;
				}
				else
				{
					ShowError(NULL, "Can't terminate process");
				}
			}
			return;
		}
		if (wParam == ProcessNotepad.NotepadWithCustomText.ID)
		{
			char szFile[240];
			OPENFILENAME openFileName;
			ZeroMemory(&openFileName, sizeof(openFileName));
			openFileName.lStructSize = sizeof(OPENFILENAME);
			openFileName.hwndOwner = hWnd;
			openFileName.lpstrFile = szFile;
			openFileName.lpstrFile[0] = '\0';
			openFileName.nMaxFile = sizeof(szFile);
			openFileName.lpstrFilter = "All files (*.*)\0*.*\0";
			openFileName.nFilterIndex = 2;
			openFileName.lpstrFileTitle = NULL;
			openFileName.nMaxFileTitle = 0;
			openFileName.lpstrInitialDir = NULL;
			openFileName.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

			if (GetOpenFileName(&openFileName))
			{
				info->CmdParams = openFileName.lpstrFile;
			}
			else if (GetLastError() != 0)
			{
				ShowError(hWnd, "Can't select file");
			}
			else
				return;
		}

		char buffer[255];
		sprintf_s(buffer, "%s %s", info->ProcessName, info->CmdParams);

		if (CreateProcess(
			info->ProcessName,
			buffer,
			nullptr,
			nullptr,
			true,
			NULL,
			nullptr,
			NULL,
			&sInfo,
			&processInformation))
		{
			info->ProcessID = processInformation.dwProcessId;
			info->ThreadID = processInformation.dwThreadId;
			info->ProcessHandle = processInformation.hProcess;
			info->ThreadHandle = processInformation.hThread;
		}
		else
		{
			ShowError(hWnd, "Cant start process.");
		}
	}
	else if (wParam >= 20 && wParam <= 29)
	{
		int index = wParam % 20;
		ProcessInfo* info = ProcessInfos[index];
		if (index == 0 && info->ProcessHandle == nullptr)
		{
			info->ProcessHandle = GetCurrentProcess();
			info->ProcessID = GetCurrentProcessId();
			info->ThreadHandle = GetCurrentThread();
			info->ThreadID = GetCurrentThreadId();
		}
		ShowProcessInfo(info);
	}
}

void ShowProcessInfo(ProcessInfo* info)
{
	if (info->ProcessHandle == nullptr)
	{
		MessageBox(NULL, "Process hasnt been created.", info->ProcessName, MB_OK);
		return;
	}
	char buffer[250];
	DWORD processExitCode;
	DWORD threadExitCode;
	GetExitCodeProcess(info->ProcessHandle, &processExitCode);
	GetExitCodeThread(info->ThreadHandle, &threadExitCode);

	char pExitCode[50] = "Actice";
	char tExitCode[50] = "Active";
	if (processExitCode != STILL_ACTIVE)
	{
		sprintf_s(pExitCode, "%d", processExitCode);
	}

	if (threadExitCode != STILL_ACTIVE)
	{
		sprintf_s(tExitCode, "%d", threadExitCode);
	}

	DWORD priorityClass = GetPriorityClass(info->ProcessHandle);

	sprintf_s(buffer,
		"Name: %s\n\rProcessID: %d\n\rThreadId: %d\n\rExit Process code : %s\n\rExit Thread code : %s\n\rPriority: %d",
		info->ProcessName,
		info->ProcessID,
		info->ThreadID,
		pExitCode,
		tExitCode,
		priorityClass);

	MessageBox(NULL, buffer, info->ProcessName, MB_OK);
	//return buffer;
}