#pragma once
#include <Windows.h>
#include "MenuForm.h"

struct MainMenuSt
{
	MenuItem Processes = { 0, "Processes" };
	MenuItem Info{ 1 ,"Info" };
};
MainMenuSt MainMenu;

struct ProcessMenuSt
{
	MenuItem Notepad = { 3, "Notepad" };
	
	MenuItem Calculator = { 14, "Calculator" };
	MenuItem Close = { 15, "Close" };
};
ProcessMenuSt ProcessMenu;

struct ProcessNotepadSt
{
	MenuItem Notepad = { 11, "Notepad" };
	MenuItem NotepadWithText = { 12, "Notepad + Source" };
	MenuItem NotepadWithCustomText = { 13, "Notepad + Custom Text" };
};
ProcessNotepadSt ProcessNotepad;

struct InfoMenuSt
{
	MenuItem CurrentProcess = { 20, "Current process" };
	MenuItem Notepad = { 2, "Notepad" };
	MenuItem Calculator = { 24, "Calculator" };
};
InfoMenuSt InfoMenu;

struct InfoNotepadSt
{
	MenuItem Notepad= { 21, "Notepad" };
	MenuItem NotepadWithText = { 22, "Notepad + Source" };
	MenuItem NotepadWithCustomText = { 23, "Notepad + Custom file" };
};
InfoNotepadSt InfoNotepad;

struct ProcessInfo
{
	HANDLE ProcessHandle;
	DWORD ProcessID;
	HANDLE ThreadHandle;
	DWORD ThreadID;
	LPCSTR ProcessName;
	LPSTR CmdParams;
};
ProcessInfo* ProcessInfos[] = 
{
	new ProcessInfo { nullptr, 0, nullptr, 0, "Lab1.exe", "" },
	new ProcessInfo { nullptr, 0, nullptr, 0, "C:\\Windows\\Notepad.exe", ""},
	new ProcessInfo { nullptr, 0, nullptr, 0, "C:\\Windows\\Notepad.exe", "Source.cpp"},
	new ProcessInfo { nullptr, 0, nullptr, 0, "C:\\Windows\\Notepad.exe", ""},
	new ProcessInfo { nullptr, 0, nullptr, 0, "C:\\Windows\\System32\\calc.exe", "" }
};
